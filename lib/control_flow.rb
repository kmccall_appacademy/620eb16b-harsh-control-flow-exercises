# EASY
require 'byebug'
# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(("a".."z").to_a.join)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length/2
  if str.length.even?
    str[mid-1..mid]
  else
    str[mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count(VOWELS.to_s)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
 (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str=""
  arr.each_with_index do |ch,idx|
    str<<ch
    str<<separator unless idx==arr.length-1
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird=str
  str.each_char.with_index {|ch,idx| ((idx+1).even?) ? weird[idx]=ch.upcase : weird[idx]=ch.downcase}
  weird
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  rev = str.split
  rev.each_index {|idx| rev[idx]=rev[idx].reverse if rev[idx].length>=5}
  rev.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fb = []
  (1..n).to_a.each do |int|
    if int%5==0 && int%3==0
      fb<<"fizzbuzz"
    elsif int%5==0
      fb<<"buzz"
    elsif int%3==0
      fb<<"fizz"
    else
      fb<<int
    end
  end
  fb

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev=[]
  arr.each {|val| rev.unshift(val)}
  rev
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num==1
  (2...num).none? {|n| num%n==0}
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).to_a.select {|n| num%n==0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|fact| prime?(fact)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr.select {|el| el.even?}.count == 1
    arr.select {|el| el.even?}.first
  else
    arr.select {|el| el.odd?}.first
  end
end
